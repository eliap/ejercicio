﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Excersice
{
    /// <summary>
    /// Lógica de interacción para Account.xaml
    /// </summary>
    public partial class Account : Window
    {
        public Account()
        {
              
            InitializeComponent();

        }



        string clave,usuario, deuda;
    

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            clave += "1";
            tbx1.Text= clave;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            clave += "2";
            tbx1.Text = clave;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            clave += "3";
            tbx1.Text = clave;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            clave += "5";
            tbx1.Text = clave;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            clave += "4";
            tbx1.Text = clave;
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            clave += "6";
            tbx1.Text = clave;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            clave += "7";
            tbx1.Text = clave;
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            clave += "8";
            tbx1.Text = clave;
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            clave += "9";
            tbx1.Text = clave;
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            clave += "0";
            tbx1.Text = clave;
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
      
            tbx1.Text = clave = clave.Remove(clave.Length - 1); ;
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            MainWindow mainw = new  MainWindow();
            mainw.Show();
            this.Close();
        }

        private void GetData(string num)
        {
            HttpClient client = new HttpClient();
          
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "text/html; charset=utf-8");
     
            client.BaseAddress = new Uri("http://linkxenter.com:3000/");

            var url = "account_balance?token=700e453283f99f57176147c2c8ce09e4&account=" + num;

            HttpResponseMessage response = client.GetAsync(url).Result;

            if (response.Content?.Headers?.ContentType != null)
            {
                response.Content.Headers.ContentType.MediaType = "application/json";
            }
            if (response.IsSuccessStatusCode)
            {
                var users = response.Content.ReadAsAsync<User>().Result;
                if (users.user== null)
                {
                    MessageBox.Show("Usuario Incorrecto, verifique y digite de nuevo");
                   
                }
                else
                {
                    usuario = users.user;
                    deuda = users.debt;
                }

            }
            else
            {
                MessageBox.Show("Error Code" + 
                response.StatusCode + " : Message - " + response.ReasonPhrase);
            }

        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            GetData(clave);
            if(usuario == null)
            {
                tbx1.Clear();
                tbx1.Focus();
                clave = "";

            }
            else
            {
                //MessageBox.Show("Clave " + clave + " Usuario " + usuario + " Deuda " + deuda);
                Balance balance = new Balance(clave, usuario, deuda);
                balance.Show();
                this.Close();
            }
           
        }
    }
}
