﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Windows;
using System.Web.Script.Serialization;

namespace Excersice
{
    /// <summary>
    /// Lógica de interacción para Payment.xaml
    /// </summary>
    public partial class Payment : Window
    {
        public Payment()
        {
            InitializeComponent();
        }
        string cuenta, user, xdeuda;
        double debt, pay, rest, cambio;
         //SqlConnection con = new SqlConnection(
        //"Data Source=YAIR-PC\\SQLEXPRESS;initial catalog= excersice; Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Encrypt=False;TrustServerCertificate=True");
        SqlConnection con = new SqlConnection("Data Source=YAIR-PC\\SQLEXPRESS;Initial Catalog=exercise;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Encrypt=False;TrustServerCertificate=True");

        private SqlCommand cmd;

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 0);
            int val, cant;
            val = 200;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 0);
            int val, cant;
            val = 100;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 0);
            int val, cant;
            val = 50;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 0);
            int val, cant;
            double cambio;
            val = 20;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 1);
            int val, cant;
            double cambio;
            val = 10;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 1);
            int val, cant;
            double cambio;
            val = 5;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 1);
            int val, cant;
            double cambio;
            val = 2;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 1);
            int val, cant;
            double cambio;
            val = 1;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 1);
            int cant;
            double val, cambio;
            val = 0.5;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(Convert.ToInt32(val), type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DeviceLibrary.DeviceLibrary device = new DeviceLibrary.DeviceLibrary();
            string depo;
            // restatxt.Content = rest;

            cambio = (rest * -1);

            //restatxt.Content = 0;
            if (cambio > 0)
            {
                MessageBox.Show("Su cambio es: " + cambio + ", Este cajero no entrega menor de 50 centavos");
                device.Dispense(Convert.ToDecimal(cambio));


            }
            device.Close();
            depo = pay.ToString();
            InsertData(depo);
            SendPost(cuenta,pay);
            MainWindow mn = new MainWindow();
            mn.Show();
            this.Close();
        }
        public void SendPost(string xaccount,double xpaid)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://linkxenter.com:3000/transaction?token=700e453283f99f57176147c2c8ce09e4");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            string xjson;

            using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    account = xaccount,
                    paid = xpaid
                });
                xjson = json;
                streamWriter.Write(json);
            }
            string xresult;
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                 xresult = result;
            }
            MessageBox.Show(xjson);
            MessageBox.Show(xresult);
        }

        public Payment(string cnta, string nom, string deuda) : this()
        {
            cuenta = cnta;
            user = nom;
            deudatxt.Content = deuda;
            debt = Convert.ToDouble(deudatxt.Content);



        }

        private void Btn500_Click(object sender, RoutedEventArgs e)
        {

            DeviceLibrary.Models.Enums.DocumentType type = (DeviceLibrary.Models.Enums.DocumentType)Enum.ToObject(typeof(DeviceLibrary.Models.Enums.DocumentType), 0);
            int val, cant;
            val = 500;
            cant = 1;
            //cant = cant + 1;
            Devicecoinbill(val, type, cant);
            pay += Convert.ToDouble(val);
            depotxt.Content = pay.ToString();
            rest = debt - pay;
            if (rest < debt)
            {
                restatxt.Content = rest;
                cambio = Convert.ToDouble(restatxt.Content);
                if (cambio < 0)
                {
                    restatxt.Content = 0;
                }
            }

        }

        public void Devicecoinbill(int value, DeviceLibrary.Models.Enums.DocumentType type, int cant)
        {
            DeviceLibrary.DeviceLibrary device = new DeviceLibrary.DeviceLibrary();
            device.Open();
            device.Enable();
            DeviceLibrary.Models.Document documento = new DeviceLibrary.Models.Document(value, type, cant);
            device.SimulateInsertion(documento);
            //device.Dispense();
            device.Disable();
            device.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (pay > 0)
            {
                MessageBox.Show("No puede cancelar abone lo insertado");
            }
            else
            {
                MainWindow mainw = new MainWindow();
                mainw.Show();
                this.Close();
            }
        }

        public void InsertData(string pago)
        {
            
            try
            {
                con.Open();
                cmd = new SqlCommand("Insert into Records (customer,account,debt,paid,fecha) Values (@customer,@account,@debt,@paid,@fecha)", con);
                cmd.Parameters.AddWithValue("@customer", user);
                cmd.Parameters.AddWithValue("@account", cuenta);
                cmd.Parameters.AddWithValue("@debt", deudatxt.Content);
                cmd.Parameters.AddWithValue("@paid", pago);
                cmd.Parameters.AddWithValue("@fecha", DateTime.Now);
                //cmd.ExecuteNonQuery();
               
               // MessageBox.Show("Record Save Successfully", "Saved", MessageBoxButton.OK);
               
                int affectedRows = cmd.ExecuteNonQuery();
                con.Close();
                //MessageBox.Show(affectedRows + " rows inserted!");
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}
