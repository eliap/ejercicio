﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Excersice
{
    /// <summary>
    /// Lógica de interacción para Balance.xaml
    /// </summary>
    public partial class Balance : Window
    {
        public Balance()
        {
            InitializeComponent();
        }
        string cuenta, user, debt;
           
        public Balance(string cnta, string nom, string deuda): this()
        {
            cuentatxt.Content = cnta;
            usuariotxt.Content = nom;
            deudatxt.Content = deuda;
            cuenta = cnta;
            user = nom;
            debt = deuda;
        }
        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            Account account = new Account();
            account.Show();
            this.Close();
        }

        private void Pagarbnt_Click(object sender, RoutedEventArgs e)
        {
           Payment payment = new Payment(cuenta, user, debt);
           payment.Show();
           this.Close();
        }
    }
}
